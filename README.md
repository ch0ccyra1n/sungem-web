# sungem-web

Web interface for the game *The Sungem* hosted on gitlab pages.


`sungem.prg` is lifted from my own game *The Sungem* which you can find more info about on [the itch.io page for the game](https://ch0ccyra1n.itch.io/sungem).

The other files in this repository are lifted from floooh's [tiny8bit](https://github.com/floooh/chips-test) project and are licensed under the MIT License.
